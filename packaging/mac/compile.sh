if [ $# -eq 0 ]
then
    echo "usage: compile.sh <SOURCE ARCHIVE>"
else
    set -e
    path=`pwd`
    archpath=`dirname $1`
    archname=`basename $1`
    name=`basename ${archname}`
    cd ${archpath}
    archpath=`pwd`
    cd ${path}
    for version in "3.7" "3.8" "3.9"
    do
        echo ${version}
        rm -fR venv-$version
        python${version} -m venv venv-${version}
        source venv-${version}/bin/activate
        cd venv-${version}
        echo ${archpath}/${archname}
        tar xzvf ${archpath}/${archname}
        dir=${name%.tar.gz}
        cd ${dir}
        python setup.py build
        python setup.py install
        python setup.py bdist_wheel
        mv dist/* ${path}/wheels
        deactivate
        cd ${path}
        rm -fR venv-${version}
    done
fi

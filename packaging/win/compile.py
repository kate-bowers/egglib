import sys, subprocess, os, shutil, re, tarfile

def run(*args):
    ret = subprocess.run(args, shell=False)
    if ret.returncode != 0:
        sys.exit('an error occurred')

# interface
if len(sys.argv) != 2:
    sys.exit(f'usage: {sys.argv[0]} <SOURCE_ARCHIVE>')
g = re.search('(EggLib-3\.\d\.\d).tar\.gz$', sys.argv[1])
if g is None: sys.exit('invalid archive name')
name = g.group(1)
print('module:', name)
if not os.path.isfile(sys.argv[1]):
    sys.exit('no such file')

for version in ['37', '38', '39']:

    # precleaning
    if os.path.isdir(f'venv-{version}'):
        shutil.rmtree(f'venv-{version}')

    # create environment
    print('creating virtual environment')
    run(rf'C:\Users\Stéphane\AppData\Local\Programs\Python\Python{version}\python.exe',
                '-m', 'venv', f'venv-{version}')

    # unpack archive and go to environment
    print('unpacking', sys.argv[1])
    t = tarfile.open(sys.argv[1])
    t.extractall(path=f'venv-{version}')
    os.chdir(os.path.join(f'venv-{version}', name))

    # running compulation script
    print('compiling')
    with open('script.bat', 'w') as f:
        script = ' & '.join([
          rf'cd ..\Scripts',
            'activate',
          rf'cd ..\{name}',
            'python setup.py build',
            'python setup.py install',
            'python setup.py bdist_wheel',
            'python --version',
            'deactivate'])
        f.write(script)
    run('script.bat')

    # collect wheel
    fnames = os.listdir('dist')
    if len(fnames) != 1:
        sys.exit('should be only one wheel file')
    os.rename(os.path.join('dist', fnames[0]), os.path.join('..', '..', 'wheels', fnames[0]))

    # go back and clean
    print('cleaning')
    os.chdir('../..')
    if os.path.isdir(f'venv-{version}'):
        shutil.rmtree(f'venv-{version}')

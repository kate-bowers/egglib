.. _toc:

========
Contents
========

.. toctree::
    :caption: Install
    :name: install
    :maxdepth: 99

    install

.. toctree::
    :caption: Manual
    :name: manual
    :maxdepth: 99

    manual/index

.. toctree::
    :caption: Statistics
    :name: statistics
    :maxdepth: 99

    stats/index

.. toctree::
    :caption: History
    :name: history
    :maxdepth: 99

    history

.. toctree::
    :caption: Licence
    :name: licence
    :maxdepth: 99

    licence

.. toctree::
    :caption: Authors
    :name: authors
    :maxdepth: 99

    authors

.. toctree::
    :caption: Reference Manual
    :name: module
    :maxdepth: 99

    py/index
    py/egglib
    py/alphabets
    py/random
    py/tools
    py/io
    py/stats
    py/coalesce
    py/wrappers

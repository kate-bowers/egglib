************
Installation
************

This page describes the installation procedure of EggLib.

------------
Requirements
------------

EggLib is tested under Python 3.7 and above (up to 3.11). Binary
distributions are available for MacOSX (10.9+) and Windows (8 or above).

If you plan to compile EggLib from source, you'll need the development 
libraries for your version of Python and the `htslib``
`library <http://www.htslib.org/download/>`_ with its headers (package
``libhts-dev`` from the Ubuntu package manager).

For using any function of the :ref:`wrappers <wrappers>` module, the
corresponding program must be found in your system and EggLib should be
configured accordingly (see :ref:`apps`).

Other requirements are Python packages ``click`` and ``platformdirs``
for the main package, ``svgwrite``, ``sphinx`` and ``numpydoc`` for
generating the documentation and ``scipy`` for running tests. They can
all be treated easily by using the package manager **pip**.

------------
Installation
------------

.. highlight:: bash

EggLib is on the `Python Package Index <https://pypi.org/project/EggLib/>`_
and can be installed using **pip** as::

    $ pip install egglib --user

The ``--user`` flag is not needed in most cases. If the permissions
don't allow you to install EggLib in a system-wide location, it will be
installed in local user libraries.

If the ``pip`` command is not directly accessible or if you want to
install EggLib for a specific (non-default) version of Python (for
example, 3.11) you can type::

    $ python3.11 -m pip install egglib --user

or, on Windows, using the Python Launcher::

    py -3.11 -m pip install egglib --user

You can also:

* Select a specific version of EggLib (for example, 3.2.0): 
  ``pip install egglib==3.2.0``.
* Upgrade to the latest version: ``pip install --upgrade egglib``.
* Remove EggLib: ``pip uninstall egglib``.

To generate the documentation, a set of packages are needed which are
not required by default. To install those, specify the ``doc`` extra::

    $ pip install egglib[doc] --user

Similarly, ``scipy`` is needed to run tests. You can either install it
yourself or let ``pip`` install it (or check it presence) as a
dependency of EggLib::

    $ pip install egglib[test] --user

Binary packages
===============

Binary packages (binary wheels) are available on **pip** for MacOSX and
Windows under Python 3.11 or above. **pip** should use the package wheel
corresponding to your system. By default, it will attempt to compile
EggLib (which is the normal behaviour on Linux).

Download source and binary packages
===================================

The source package can be downloaded from **pip** using::

    $ pip download egglib --no-binary :all:

This will download the source package. Letting you access the source 
code.

To download the binary package corresponding to your system (assuming it
is available), you can just type::

    $ pip download egglib

Alternatively, all files for the current release are available for
download from `<https://pypi.org/project/EggLib/#files>`_. Older
releases are also archived there.

.. _apps:

---------------------------------
Configuring external applications
---------------------------------

If external applications are needed (one is required for every function
of the :ref:`wrappers <wrappers>` module), they must be configured. By
default, EggLib will assume that the corresponding programs are absent
and will not attempt to run them.

Since version 3.2, a companion script in included in the Egglib
installation to manage external application paths. To test for the
presence of external applications and save the configuration in a user
configuration file, issue the following command::

    $ egglib-config apps -aLsu

* Option ``-a`` launchs autodetection of applications using default
  command names for invoking them (e.g. ``phyml`` for the PhyML
  software). If needed, for any application, custom commands can be
  specified using the ``-c`` option and path to executables can be
  specified using  ``-p``. If a command tries by ``-a`` doesn't work,
  this application will be left unavailable.

* Option ``-L`` displays the result.

* Option ``-s`` saves the configuration to a persistent file such as the
  new configuration will be used by EggLib after subsequent imports.

* Option ``-u`` specifies that the configuration file will be saved in
  a user-specific configuration file. By default, the configuration file
  is located within the EggLib installation and is erased when EggLib is
  updated and might require administrator rights for writing. If the
  user-specific file exists, it is used in priority (so ``-u`` only
  needs to be specified when it doesn't exist yet).

See the manual (``egglib-config apps --help``) for more information.

It is also possible to perform these operations using helpers of the
:mod:`!wrappers` module, either permanently or temporarily. For details,
see :ref:`paths`.

-----
Tests
-----

Since version 3.2, the test suite is included as a subpackage of EggLib
and can be invoked through a companion script named ``egglib-test``.
The following command will run all tests and assumes all components
(external applications and `htslib`` for the :class:`.io.VCF` class) are
available::

    $ egglib-test -a

It is possible to run more limited tests (in particular, skip the 
:ref:`wrappers <wrappers>` module). See ``egglib-test --help`` for more 
details.

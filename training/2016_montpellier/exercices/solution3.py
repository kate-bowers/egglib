import egglib

###
print '### exercice 3.1.1 ###'
data = egglib.Align(640, 0, 30, -1)
###

###
print '### exercice 3.1.2 ###'
f = open('data1.txt')
for i, line in enumerate(f):
    data[i].sequence = map(int, line.split())
f.close()
###

###
print '### exercice 3.2.1 ###'
cs = egglib.stats.ComputeStats()
cs.configure(filtr=egglib.stats.filter_ssr,
             ignore_multiallelic=False)
###

###
print '### exercice 3.2.2 ###'
struct = egglib.stats.Structure.make_from_dict(
    pops={0: range(0, 160),
          1: range(160, 320),
          2: range(320, 480),
          3: range(480, 640)})
cs.set_structure(struct)
###

###
print '### exercice 3.2.3 ###'
cs.add_stat("A")
cs.add_stat("He")
cs.add_stat("R")
cs.add_stat("WCst")
cs.add_stat("rD")
print cs.process_align(data)
###

###
print '### exercice 3.2.4 ###'
cs.configure(filtr=egglib.stats.filter_ssr,
             ignore_multiallelic=False,
             average_stats=False)
cs.set_structure(struct)
cs.add_stat("A")
cs.add_stat("He")
cs.add_stat("R")
cs.add_stat("WCst")
cs.add_stat("rD")
print cs.process_align(data)
###

###
print '### exercice 3.2.5 ###'
struct = egglib.stats.Structure.make_from_dict(
    pops={0: range(0, 80),
          1: range(80, 160),
          2: range(160, 240),
          3: range(240, 320)},
    indivs_ingrp=dict([(i, (2*i, 2*i+1)) for i in range(320)]),
    indivs_outgrp={})
###

###
print '### exercice 3.2.6 ###'
cs.configure(filtr=egglib.stats.filter_ssr,
             ignore_multiallelic=False)
cs.set_structure(struct)
cs.add_stat("Fis")
cs.add_stat("Gst")
cs.add_stat("WCst")
cs.add_stat("WCist")
print cs.process_align(data)
###

###
print '### exercice 3.3.1 ###'
vcf = egglib.io.VcfParser('genome.vcf')
print 'number of samples:', vcf.num_samples
print 'samples:', [vcf.get_sample(i) for i in range(vcf.num_samples)]
###

###
print '### exercice 3.3.2 ###'
print vcf.next()
variant = vcf.last_variant()
print type(variant)
###

###
print '### exercice 3.3.3 ###'
print 'chromosome:', variant.chromosome
print 'position:', variant.position
print 'number of alleles:', variant.num_alleles
print 'alleles:', variant.alleles
print 'AA:', variant.AA
print 'AC:', variant.AC
print 'AN:', variant.AN
print 'info:', variant.info
print 'depth:', variant.info['DP']
###

###
print '### exercice 3.3.4 ###'
print 'number of items in GT:', len(variant.GT)
print 'first item:', variant.GT[0]
print 'third item:', variant.GT[2]
###

###
print '### exercice 3.4.1 ###'
sites = vcf.get_genotypes(get_genotypes=True)
print type(sites)

###
print '### exercice 3.4.2 ###'
print 'number of samples:', sites.ns
print 'number of alleles:', sites.num_alleles
print 'frequencies:', sites.freqs()
print 'genotypes:', sites.alleles()
print 'num genotypes:', len(sites.alleles())
###

###
print '### exercice 3.4.3 ###'
cs.configure()
cs.add_stat("He")
cs.add_stat("Fis")
cs.add_stat("A")
cs.add_stat("ns_site")
print cs.process_site(sites)
###

###
print '### exercice 3.4.4 ###'
for i in range(22):
    chrom, pos, nall = vcf.next()
    sites = vcf.get_genotypes(get_genotypes=True)
    print chrom, pos, nall, sites.freqs()[0], cs.process_site(sites)
###

###
print '### exercice 3.4.5 ###'
variant = vcf.last_variant()
print variant.alleles
print sites.alleles()
###

###
print '### exercice 3.5.1 ###'
array = egglib.stats.SiteArray()
c = 0
for chrom, pos, nall in vcf:
    c += 1
    site = vcf.get_genotypes(get_genotypes=True)
    array.import_site(site)
print 'processed sites:', c
print 'imported sites:', len(array)
###

###
print '### exercice 3.5.2 ###'
cs.configure()
struct = egglib.stats.Structure.make_from_dict(
    indivs_ingrp=dict([(i, (2*i, 2*i+1)) for i in xrange(90)]),
    indivs_outgrp={})
cs.set_structure(struct)
cs.add_stat("S")
cs.add_stat("nseff")
cs.add_stat("ls")
cs.add_stat("ls_o")
cs.add_stat("D")
cs.add_stat("Hsd")
cs.add_stat("Fis")
print cs.process_align(array)
###

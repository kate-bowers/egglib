import egglib

###
print "### exercice 4.1.1 ###"
sim = egglib.coalesce.Simulator(num_pop=1)
###

###
print "### exercice 4.1.2 ###"
sim.params['theta'] = 5.0
sim.params['num_chrom'] = [40]
print sim.params
###

###
print "### exercice 4.2.1 ###"
aln = sim.simul()
print type(aln)
###

###
print "### exercice 4.2.2 ###"
for aln in sim.iter_simul(1000):
    print aln.to_fasta(mapping='01')
###

###
print "### exercice 4.3.1"
sim = egglib.coalesce.Simulator(num_pop=2, migr=2., theta=2.5, num_chrom=[20,20])
print sim.params
###

###
print "### exercice 4.3.2"
sim.params['migr_matrix'] = [[None,2.],[1.,None]]
print sim.params
###

###
print "### exercice 4.4.1"
sim = egglib.coalesce.Simulator(num_pop=2,migr_matrix=[[None,2.],[1.,None]],theta=2.5,num_chrom=[20,20])
sim.params['num_sites'] = 1000
aln = sim.simul()
cstats = egglib.stats.ComputeStats()
cstats.configure(filtr=egglib.stats.filter_default)
cstats.add_stat('S')
stats = cstats.process_align(aln)
print aln.ls, stats['S']
###

###
print "### exercice 4.5.1"
for stat in sim.iter_simul(1000,cs=cstats):
    print stat['S']
###

###
print "### exercice 4.5.2"
d = {0: range(20), 1: range(20, 40)}
structure = egglib.stats.Structure.make_from_dict(pops=d)
cstats.set_structure(structure)
cstats.add_stat('WCst')
for stat in sim.iter_simul(1000,cs=cstats):
    print stat['WCst']

###
print "### exercice 4.6.1"
sim = egglib.coalesce.Simulator(num_pop=1)
sim.params['num_chrom'] = [40]
sim.params['theta'] = 5.
sim.params['recomb'] = 5.
sim.params['events'].add(cat='bottleneck',T=0.3,S=10.)
cstats = egglib.stats.ComputeStats()
cstats.configure(filtr=egglib.stats.filter_default)
cstats.add_stat('D')

D_bott = []
for stats in sim.iter_simul(nrepet=10000,cs=cstats):
    D_bott.append(stats['D'])
###

###
print "### exercice 4.6.2"
from matplotlib import pyplot
sim.params['events'].clear()
D_neut = []
for stats in sim.iter_simul(nrepet=10000,cs=cstats):
    D_neut.append(stats['D'])
###

###
print "### exercice 4.6.3"
pyplot.hist([D_bott,D_neut],50,histtype='bar',color=['blue', 'red'],label=['Bottleneck','Neutre'])
pyplot.legend()
pyplot.xlabel(r'$\mathit{D}$')
pyplot.savefig("figure3.png")
pyplot.clf()
###



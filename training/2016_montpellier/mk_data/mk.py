import egglib, operator, random

# parameters
kappa = 2.6     # transition / transversion rate ratio
w = 0.44        # dN/dS mutation ratio (strength of purifying selection)
ns = [25, 22, 22, 30]
no = 2
theta = 0.005
migr = 2.1
recomb = 6.0
tfusion = 7.0
transversions = ['AC', 'CA', 'AT', 'TA', 'GC', 'CG', 'GT', 'TG']
ngaps = 50
missing = 0.01
npop = len(ns)
totns = sum(ns)

# define exon structure
exons_len = 147, 865, 234, 779    # as number of aa
introns_len = 978, 1244, 645
cds = sum(exons_len)
assert cds % 3 == 0
assert len(introns_len) == len(exons_len) - 1

# build a codon transition matrix (only non-stop codons)
T = egglib.tools.Translator(code=1)
codons = []
for i in range(63):
    codon = egglib.tools.int2codon(i)
    aa = T.translate_codon(*codon)
    if aa != '*':
        codons.append((codon, aa))
dim = len(codons)
codon_matrix = [[None] * dim for i in xrange(dim)]

for i, (codon1, aa1) in enumerate(codons):
    for j, (codon2, aa2) in enumerate(codons):
        if i == j:
            codon_matrix[i][j] = None
        else:
            if aa1 == aa2: term1 = 1.0
            else: term1 = w
            ndiff = 3 - sum(map(operator.eq, codon1, codon2))
            if ndiff > 1:
                codon_matrix[i][j] = 0.0
            else:
                for a, b in zip(codon1, codon2):
                    if a != b:
                        if a+b in transversions:
                            term2 = 1.0 # these are transversions
                        else:
                            term2 = kappa
                        break
                else:
                    raise RuntimeError
            codon_matrix[i][j] = term1 * term2

# make a nucleotide transition matrix
nucleotides = 'ACGT'
nucl_matrix = [[None] * 4 for i in xrange(4)]
for i in xrange(4):
    for j in xrange(4):
        if i == j:
            nucl_matrix[i][j] = None
        else:
            a = nucleotides[i]
            b = nucleotides[j]
            if a+b in transversions:
                nucl_matrix[i][j] = 1.0
            else:
                nucl_matrix[i][j] = kappa

# configure Simulator for CDS
sim = egglib.coalesce.Simulator(npop=npop+1, migr=migr)
sim.params['num_chrom'][:npop] = ns
sim.params['num_chrom'][npop] = no
sim.params['theta'] = theta * cds
sim.params['num_sites'] = cds
sim.params['num_alleles'] = len(codons)
sim.params['trans_matrix'] = codon_matrix
sim.params['rand_start'] = True
for i in xrange(npop):
    sim.params['migr_matrix'][i, npop] = 0.0
    sim.params['migr_matrix'][npop, i] = 0.0
for i in xrange(npop):
    sim.params['events'].add('merge', tfusion, src=i+1, dst=0)

# simulate the exons
exon_seq = sim.simul()

# make a copy with expanded codons
exon_cpy = egglib.Align(nsam=totns+no, nsit=3*cds)
exon_cpy.ng = 2
for sam1, sam2 in zip(exon_seq, exon_cpy):
    sam2.group = sam1.group

for i, sam in enumerate(exon_seq):
    for j, codon in enumerate(sam.sequence):
        exon_cpy[i].sequence[3*j:3*j+3] = map(ord, codons[codon][0])
exons = []
acc = 0
for ln in exons_len:
    exons.append(exon_cpy.extract(acc, acc+ln*3))
    acc += ln*3

# re-configure Simulator for introns
sim.params['num_sites'] = sum(introns_len)
sim.params['num_alleles'] = 4
sim.params['trans_matrix'] = nucl_matrix
sim.params['theta'] = theta * sum(introns_len)

# simulate the introns (using private methods to use the same genealogical tree)
sim._coalesce.mutate()
intron_seq = egglib.Align._create_from_data_holder(sim._coalesce.data())

# recode integer values and extract exons
for sam in intron_seq:
    for j, v in enumerate(sam.sequence):
        sam.sequence[j] = ord(nucleotides[v])

segments = exons
acc = 0
for i, ln in enumerate(introns_len):
    segments.insert(1+2*i, intron_seq.extract(acc, acc+ln))
    acc += ln

# introduce gaps
cache = set()
rnd = egglib.tools.Random()
c = 0
tot = 0
while c < ngaps:
    tot += 1
    if tot > 500: break
    idx = random.choice(xrange(len(segments)))
    if idx % 2 == 0: ln = random.choice([1,2,3,4,5]) * 3
    else: ln = random.choice([1,2,3,5,7,10,20,30,50,100])
    pt = random.choice(xrange(segments[idx].ls))
    fw = rnd.boolean()
    if fw: start, stop = pt, pt+ln
    else: start, stop = pt-ln+1, pt+1
    if start < 0: start = 0
    if stop > segments[idx].ls: stop = segments[idx].ls
    pos = set(range(start, stop))
    if len(cache & pos) > 0: continue
    cache.update(pos)
    site = egglib.stats.SiteFrequency.make_from_dataset(segments[idx], pt)
    alls = site.alleles(True)
    alls = set(alls[0] + alls[1])
    if len(alls) == 1: continue
    which = random.choice(list(alls))
    for sam in segments[idx].iter(True, True):
        if sam.sequence[pt] == which:
            sam.sequence[start:stop] = [ord('-')] * (stop - start)
    c += 1

# generate the final align
aln = egglib.tools.concat(segments, ignore_names=True)
for i, sam in enumerate(aln):
    sam.name = 'sample_{0:0>2}'.format(i+1)
for i in xrange(no):
    aln.to_outgroup(totns)

# introduce missing data
pos = []
for i in xrange(aln.ns):
    for j in range(aln.ls):
        pos.append((i, j))
n = int(round(missing * len(pos)))
for i, j in random.sample(pos, n):
    if aln.get(i, j) != ord('-'): aln.set(i, j, ord('N'))
pos = []
for i in xrange(aln.no):
    for j in range(aln.ls):
        pos.append((i, j))
n = int(round(missing * len(pos)))
for i, j in random.sample(pos, n):
    if aln.get_o(i, j) != ord('-'): aln.set_o(i, j, ord('N'))

# define the reading frame
frame = []
acc = 0
for i, ln in enumerate(exons_len):
    frame.append((acc, acc+ln*3))
    acc += ln*3
    if i< len(introns_len): acc += introns_len[i]
    else: acc = None # prevent further increment

print aln.ls

# save data
aln.to_fasta(fname='align1.fas', groups=True)
f = open('frame.txt', 'w')
for a, b in frame: f.write('{0} {1}\n'.format(a+1, b+1))
f.close()

# second long alignment with recombination
sim2 = egglib.coalesce.Simulator(npop=1)
sim2.params['num_chrom'][0] = 100
sim2.params['theta'] = theta * 50000
sim2.params['num_sites'] = 50000
sim2.params['num_alleles'] = 4
sim2.params['rand_start'] = True
sim2.params['recomb'] = recomb
aln2 = sim2.simul()
aln2.to_fasta(fname='align2.fas', mapping=nucleotides)

\select@language {french}
\contentsline {section}{\numberline {1}Exercices 1 -- Prise en main d'EggLib}{5}{section.1}
\contentsline {subsection}{\numberline {1.1}Importer un alignment depuis un fichier Fasta}{5}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Manipuler les \'echantillons}{6}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}\'Editer les s\'equences}{7}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}G\'erer les labels de structure}{8}{subsection.1.4}
\contentsline {subsection}{\numberline {1.5}Acc\`es direct aux donn\'ees}{9}{subsection.1.5}
\contentsline {subsection}{\numberline {1.6}Exporter les donn\'ees au format Fasta}{9}{subsection.1.6}
\contentsline {subsection}{\numberline {1.7}G\'erer des s\'equences non align\'ees}{10}{subsection.1.7}
\contentsline {subsection}{\numberline {1.8}Ajouter et supprimer des \'echantillons}{10}{subsection.1.8}
\contentsline {subsection}{\numberline {1.9}Extraire des sections d'un alignement}{11}{subsection.1.9}
\contentsline {subsection}{\numberline {1.10}Manipuler des sous-alignements}{11}{subsection.1.10}
\contentsline {subsection}{\numberline {1.11}Traduire des s\'equences codantes}{12}{subsection.1.11}
\contentsline {section}{\numberline {2}Exercices 2 -- Calcul de statistiques de diversit\'e nucl\'eotidique}{13}{section.2}
\contentsline {subsection}{\numberline {2.1}Calcul de statistiques de base}{13}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}Prise en compte de la structure}{14}{subsection.2.2}
\contentsline {subsection}{\numberline {2.3}Gestion des donn\'ees manquantes}{14}{subsection.2.3}
\contentsline {subsection}{\numberline {2.4}Analyse de la matrice de d\'es\'equilibre de liaison}{15}{subsection.2.4}
\contentsline {subsection}{\numberline {2.5}Statistiques synonymes et non-synonymes}{16}{subsection.2.5}
\contentsline {section}{\numberline {3}Exercices 3 -- Analyse de microsatellites et de donn\'ees g\'enomiques}{18}{section.3}
\contentsline {subsection}{\numberline {3.1}Importer des donn\'ees microsatellites}{18}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Calculer des statistiques de diversit\'e}{18}{subsection.3.2}
\contentsline {subsection}{\numberline {3.3}Lire des donn\'ees VCF}{20}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Calculer des statistiques depuis un site d'un VCF}{21}{subsection.3.4}
\contentsline {subsection}{\numberline {3.5}Analyse d'un bloc de sites de VCF}{22}{subsection.3.5}
\contentsline {section}{\numberline {4}Exercices 4 -- Simulations de coalescence}{23}{section.4}
\contentsline {subsection}{\numberline {4.1}Cr\'eation de l'objet et initialisation des param\`etres}{23}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}Effectuer les simulations}{23}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Passage \`a plusieurs populations}{24}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Simulations en nombre de sites fixe}{24}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}Couplage simulations/calcul de statistiques de polymorphisme}{24}{subsection.4.5}
\contentsline {subsection}{\numberline {4.6}\'Ev\`enements d\'emographiques}{25}{subsection.4.6}
\contentsline {section}{\numberline {5}Exercices 5 -- Approximate Bayesian Computation}{26}{section.5}
\contentsline {subsection}{\numberline {5.1}Calcul des statistiques r\'esumantes sur donn\'ees r\'eelles}{26}{subsection.5.1}
\contentsline {subsection}{\numberline {5.2}Simulation sous chacun des 3 mod\`eles}{27}{subsection.5.2}
\contentsline {subsection}{\numberline {5.3}Fit de mod\`eles avec le package abc de R}{28}{subsection.5.3}
\contentsline {section}{\numberline {6}Exercices 6 -- Analyse phylog\'en\'etique \`a l'aide de programmes externes}{30}{section.6}
\contentsline {subsection}{\numberline {6.1}Configuration des chemins d'acc\`es aux programmes}{30}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Importer des {\color {eggorange}{\textbf {\texttt {Align}}}} dans un dictionnaire}{31}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Reconstruire un arbre}{31}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Identifier un clade dans un arbre}{32}{subsection.6.4}
\contentsline {subsection}{\numberline {6.5}R\'eorienter l'arbre}{33}{subsection.6.5}
\contentsline {subsection}{\numberline {6.6}Extraire et marquer les sous-arbres}{34}{subsection.6.6}
\contentsline {subsection}{\numberline {6.7}Recherche des signatures de s\'election positive}{35}{subsection.6.7}

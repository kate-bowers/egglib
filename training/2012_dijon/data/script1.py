import os

count = 0
names = set()

files = os.listdir('fasta')

print 'Number of files:', len(files)


for fname in files:
    f = open('fasta/' + fname)

    for line in f:

        if line[0] == '>':
            name = line[1:]
            name = name.rstrip()
            names.add(name)
            count += 1
            
print 'Total number of sequences:', count
print 'Number of unique sequences:', len(names)



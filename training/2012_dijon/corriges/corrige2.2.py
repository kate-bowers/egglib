import os

nA = 0
nC = 0
nG = 0
nT = 0
nTotal = 0

files = os.listdir('fasta')

for fname in files:
    f = open('fasta/' + fname)
    for line in f:
        if line[0] != '>':
            line = line.strip()
            nA += line.count('A')
            nC += line.count('C')
            nG += line.count('G')
            nT += line.count('T')
            nTotal += len(line)
    f.close()
        
print 'Average number of A:', nA
print 'Average number of C:', nC
print 'Average number of G:', nG
print 'Average number of T:', nT
print 'Average number of other characters:', nTotal - (nA+nC+nG+nT)

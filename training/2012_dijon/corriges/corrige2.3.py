import os

if os.path.isdir('fasta2') == True:
    for i in os.listdir('fasta2'):
        os.remove('fasta2/' + i)
else:
    os.mkdir('fasta2')

files = os.listdir('fasta')

for fname in files:
    f1 = open('fasta/' + fname)
    f2 = open('fasta2/' + fname, 'w')

    for line in f1:
        if line[0] == '>':
            f2.write(line)

        else:
            line = line.strip()
            for i in line:
                if i in 'ACGT':
                    f2.write(i)
                else:
                    f2.write('N')
            f2.write('\n')

    f1.close()
    f2.close()

python -m egglib.utils codalign input=sequences.fas output=align.fas
python -m egglib.utils extract input=align.fas output=debut.fas ranges=1-800
python -m egglib.utils phyml input=debut.fas output=tree2.tre
python -m egglib.utils reroot input=tree1.tre output=tree1-r.tre outgroup=AtCERK1
python -m egglib.utils reroot input=tree2.tre output=tree2-r.tre outgroup=AtCERK1

import os, egglib

files = os.listdir('fasta')

for fname in files:
    aln = egglib.Align('fasta/' +  fname, groups=True)
    aln2 = egglib.Align()

    groups = aln.groups()

    aln1 = egglib.Align()
    for name in groups[1]:
        aln1.append(name, aln.sequenceByName(name))
    pol1 = aln1.polymorphism(minimumExploitableData=0.5)

    aln2 = egglib.Align()
    for name in groups[2]:
        aln2.append(name, aln.sequenceByName(name))
    pol2 = aln2.polymorphism(minimumExploitableData=0.5)

    print fname, 'Pop1 S:', len(aln1), ' - Pop1 D:', pol1['D'], ' - Pop2 S:', len(aln2), ' - Pop2 D:', pol2['D']

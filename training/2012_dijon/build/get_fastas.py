import egglib, os

path = '/home/demita/Documents/eagle/analyses/fas'

for i in os.listdir(path):
    aln1 = egglib.Align(path + '/' + i, groups=True)
    aln2 = egglib.Align()
    n1 = 0
    for seq in aln1:
        if seq.group==0:
            aln2.append(seq.name, seq.sequence, 1)
            n1 += 1
    n2 = 0
    for seq in aln1:
        if seq.group==1:
            aln2.append(seq.name, seq.sequence, 2)
            n2 += 1
    aln2.write('fasta/{}.fas'.format(i), True)
    print aln1.ns(), aln2.ns(), n1, n2

import egglib

vcf = egglib.io.VcfParser('CEU.exon.2010_03.genotypes.vcf')
print(vcf.num_samples)
print([vcf.get_sample(i) for i in range(vcf.num_samples)])
vcf.readline()
site = vcf.get_genotypes()
print(site.as_list())


vcf.rewind()
site = egglib.Site()
for (ch, pos, na) in vcf:
    vcf.get_genotypes(dest=site)
    print(site.as_list())


import gzip
f = gzip.open('chr4B.mod.vcf.gz','rt')
cache = []
while True:
    line = f.readline()
    if line[:2] == '##': cache.append(line)
    elif line[:1] == '#':
        cache.append(line)
        break
    else: raise IOError('invalid file')
header = ''.join(cache)
print(header)
vcf = egglib.io.VcfStringParser(header)

line = f.readline()
print(vcf.readline(line.rstrip())) # rstrip because file malformed
site = vcf.get_genotypes()
print(site.as_list())


pops = [[0, 1, 2, 3, 4, 10, 11, 12, 13, 14, 18, 19],
        [20, 21, 22, 23, 25, 26, 32, 33, 34, 35, 36]]
struct_d = { None: {
    f'pop{pop+1}':
        {f'idv{idv+1}': (2*index, 2*index+1)
                for (idv, index) in enumerate(indivs)}
                    for (pop, indivs) in enumerate(pops)}}
struct = egglib.struct_from_dict(struct_d, None)
print(struct.as_dict())

print('### sliding window')
cs = egglib.stats.ComputeStats(struct=struct)
cs.add_stats('thetaW', 'D')
vcf = egglib.io.VcfParser('chr4B.mod.vcf')
sld = vcf.slider(100000, 100000, stop=5000000, max_missing=50)
for win in sld:
    print(win.bounds, len(win), cs.process_sites(win))

import egglib

### 1.1

print "### exercise 1.1.1 ###"
aln = egglib.io.from_fasta("align1.fas")

print "### exercise 1.1.2 ###"
print "number of ingroup samples:", aln.ns, "=", len(aln)
print "number of outgroup samples:", aln.no
print "length of alignment:", aln.ls

print "### exercise 1.1.3 ###"
print "first name:", aln.get_name(0)
aln = egglib.io.from_fasta("align1.fas", groups=True)
print "first name:", aln.get_name(0)
print "number of ingroup samples:", aln.ns
print "number of outgroup samples:", aln.no

### 1.2

print "### exercise 1.2.1 ###"
first = aln[0]
print 'name of first sample:', first.name

print "### exercise 1.2.2 ###"
print 'name of ingroup samples:'
for sam in aln:
    print '   ', sam.name

print "### exercise 1.2.3 ###"
print 'name of outgroup samples:'
for sam in aln.iter_outgroup():
    print '   ', sam.name

print "### exercise 1.2.4 ###"
print 'name of 1st sample:', first.name
first.name = "the first sample"
print 'new name of 1st sample:', first.name
print 'check from Align:', aln.get_name(0)

### 1.3

print "### exercise 1.3.1 ###"
print 'type of alignment:', type(aln)
print 'type of sample:', type(first)
print 'type of sequence:', type(first.sequence)

print "### exercise 1.3.2 ###"
print '1st sequence as string:'
print first.sequence.string()

print "### exercise 1.3.3 ###"
print '1st base of 1st sequence:', first.sequence[0]
print '10 first bases of 1st sequence:', first.sequence[:10]

print "### exercise 1.3.4 ###"
first.sequence[0] = 'N'
first.sequence[0] = 78 # equal to ord('N')

print "### exercise 1.3.5 ###"
first.sequence[:10] = 'CGGAGAGCCA'
try:
    first.sequence[:10] = 'CGGAGAGCCAAAA'
except ValueError, e:
    print 'expected error caught with message:', e.message
    # this syntax allows to block a given error

print "### exercise 1.3.6 ###"
print 'get name:', aln.get_name(0)
print 'get sequence:', aln.get_sequence(0).string()
print 'get label:', aln.get_label(0, 0)
print 'get base:', aln.get_i(0, 0)
aln.set_i(0, 0, 'T')
print 'after change base:', aln.get_i(0, 0)

### 1.4

print "### exercise 1.4.1 ###"
aln2 = egglib.Align()
sam = aln[3]
aln2.add_sample(sam.name, sam.sequence)
del aln[3]
print 'ns original alignment:', aln.ns
print 'ns new alignment:', aln2.ns

print "### exercise 1.4.2 ###"
sub1 = aln.extract(100, 201)
print "1st fragment - ns:" , sub1.ns, "ls:", sub1.ls

print "### exercise 1.4.3 ###"
sub1.to_fasta('out/sub.fas')

print "### exercise 1.4.4 ###"
sub2 = aln.extract([0, 10, 20, 25, 30, 50])
print "2nd fragment - ns:" , sub2.ns, "ls:", sub2.ls

print "### exercise 1.4.5 ###"
sub3 = aln.subset([0, 1, 2, 5, 10, 15, 20, 22, 25, 28])
print "3rd fragment - ns:" , sub3.ns, "ls:", sub3.ls

print "### exercise 1.4.6 ###"
half1 = aln.extract(0, 4471)
half2 = aln.extract(4471, None)
print 'ls 1st half:', half1.ls
print 'ls 2nd half:', half2.ls
aln2 = egglib.tools.concat(half1, half2, spacer=1000)
print 'ls merged halves with spacer:', aln2.ls
print 'ls original alignment:', aln.ls

### 1.5

print "### exercise 1.5.1 ###"
pos = [(0, 441), (1419, 4014), (5258, 5960), (6605, 8942)]
rf = egglib.tools.ReadingFrame(pos)
prot = egglib.tools.translate(aln, frame=rf)
prot.to_fasta('out/prot.fas')
print 'translated alignment - ns:', prot.ns, 'ls:', prot.ls

print "### exercise 1.6.2 ###"
print 'number of trailing stop codons:', egglib.tools.trailing_stops(aln, frame=rf)
print 'test for stop codons in alignment:', egglib.tools.has_stop(aln, frame=rf)

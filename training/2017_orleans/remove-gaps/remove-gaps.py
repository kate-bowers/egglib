import egglib, random

A, C, G, T, N, X = map(ord, 'ACTGN-')

aln = egglib.io.from_fasta('align0.fas')
all_bases = range(aln.ls)

# process all codon positions
pos = [(0, 441), (1419, 4014), (5258, 5960), (6605, 8942)]
rf = egglib.tools.ReadingFrame(pos)
for i,j,k in rf.iter_codons():
    repl = []
    valid = []
    for idx, sam in enumerate(aln):
        cod = sam.sequence[i:k+1]
        if N in cod or X in cod:
            repl.append(idx)
        else:
            valid.append(cod)
    valid = random.choice(valid)
    for idx in repl:
        aln[idx].sequence[i:k+1] = valid
    all_bases.remove(i)
    all_bases.remove(j)
    all_bases.remove(k)

# clean missing data in other bases
for i in all_bases:
    repl = []
    valid = []
    for idx, sam in enumerate(aln):
        base = sam.sequence[i]
        if base == N or base == X:
            repl.append(idx)
        else:
            valid.append(base)
    valid = random.choice(valid)
    for idx in repl:
        aln[idx].sequence[i] = valid

aln.to_fasta('../exo1/align1.fas')

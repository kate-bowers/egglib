import egglib, random, pickle

vcf = egglib.io.VcfParser('chr2_phase3_frag.vcf')
print 'number of samples:', vcf.num_samples
samples = [vcf.get_sample(i) for i in xrange(vcf.num_samples)]

vcf2 = egglib.io.VcfParser('chr2_phase3_thin.vcf')
samples2 = [vcf2.get_sample(i) for i in xrange(vcf2.num_samples)]
assert samples == samples2


sample_data = {}
f = open('data.txt')
h = f.readline().strip().split('\t')
for line in f:
    line = line.strip().split('\t')
    assert len(line) == len(h)
    line = dict(zip(h, line))
    k = line['Sample']
    del line['Sample']
    sample_data[k] = line

families = {}
for sam in samples:
    fam = sample_data[sam]['Family ID']
    if fam not in families: families[fam] = [[], set()]
    families[fam][0].append(sam)
    families[fam][1].add(sample_data[sam]['Population'])

structure = {0: {}}
labels = []
c = 0

print 'number of samples in database:', len(sample_data)
print 'number of families:', len(families)

for fam in families:
    assert len(families[fam][1]) == 1
    lbl = families[fam][1].pop()
    families[fam] = random.choice(families[fam][0])
    if lbl=='CEU' or lbl=='JPT':
        if lbl not in labels:
            labels.append(lbl)
            structure[0][labels.index(lbl)] = {}
        structure[0][labels.index(lbl)][c] = [c]
        c += 1

print 'final number of samples:', c
print 'number of populations:', len(labels)
print labels

pickle.dump(structure, open('structure.p', 'w'))

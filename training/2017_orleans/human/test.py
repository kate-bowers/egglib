import egglib, pickle

vcf = egglib.io.VcfParser('chr2_phase3_thin.vcf')
struct = egglib.stats.make_structure(ingroup=pickle.load(open('structure.p')), outgroup=None)

sld = vcf.slider(100, 10, miss_pv=100)

cs = egglib.stats.ComputeStats()
cs.add_stats('WCst')

X = []
Y = []

while sld.good:
    sld.next()
    print '{0}:{1:.1f}Mbp'.format(sld.chromosome, sld.start_bound/1000000.0)
    X.append(sld.start_bound + (sld.end_bound-sld.start_bound)*0.5)
    Y.append(cs.process_sites(sld, struct=struct)['WCst'])


from matplotlib import pyplot

pyplot.plot(X, Y, 'ko', mec='k', mfc='None')
pyplot.savefig('test.png')

class matrix:
    def __init__(self, dim):
        self._list = [[None]* dim for i in range(dim)]
    
    def show(self):
        for i in self._list:
            print '\t'.join(map(str, i))
    
    def get(self, i, j):
        return self._list[i][j]
    
    def set(self, i, j, value):
        self._list[i][j] = value


mat = matrix(3)
mat.show()
for i in range(3):
    for j in range(3):
        mat.set(i, j, (i,j))

mat.show()

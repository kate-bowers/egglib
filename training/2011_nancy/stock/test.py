import egglib

tree = egglib.Tree("arbre.tre")

root = tree.findGroup(["Selaginella", "Physcomitrella"])
print root
print root.leaves_down()

treetemp = tree.copy()
root = treetemp.findGroup(["Selaginella", "Physcomitrella"])
treetemp.reoriente(root)
treetemp.write("arbre3.tre")

treetemp = tree.copy()
root = treetemp.findGroup(["Selaginella", "Physcomitrella"])
treetemp.root(root)
treetemp.write("arbre4.tre")

treetemp = tree.copy()
root = treetemp.findGroup(["Selaginella", "Physcomitrella"])
treetemp.root(root, 0.8)
treetemp.write("arbre5.tre")


tree.midroot()
tree.write("arbre6.tre")


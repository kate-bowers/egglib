# Manipulation basique de listes

liste = [415, 64, 28, 351, 616]
print liste[0]
print liste[-1]
print liste[2:5]
print liste[-3:]
liste.append(822)
liste.sort()
liste.reverse()
print liste.index(415)
liste = map(float, liste)
liste2 = filter(lambda x: x<60, liste)

# Utilisation de sets

set1 = set("ABCDEF")
set2 = set("ACEGH")
print set1 & set2
print set1 | set2

# Comptage de paires aleatoires

import random
liste1 = [random.randint(0, 10) for i in range(1000)]
liste2 = [random.randint(0, 10) for i in range(1000)]
paires = zip(liste1, liste2)
uniq = set(paires)
counts = dict.fromkeys(uniq)
for i in uniq:
    counts[i] = paires.count(i)
print counts

# Utilisation d'un fichier de donnees et extraction d'information

f = open('example1.fas')
for i in range(10):
    header = f.readline()
    header = header.strip()
    name = header[1:]
    sequence = f.readline()
    sequence = sequence.strip()
    cA = sequence.count('A')
    cC = sequence.count('C')
    cG = sequence.count('G')
    cT = sequence.count('T')
    cOthers = len(sequence) - (cA+cC+cG+cT)
    print name, cA, cC, cG, cT, cOthers
f.close()

f = open('example1.fas')
outfile = open('example1.txt', 'w')
for i in range(10):
    header = f.readline()
    header = header.strip()
    name = header[1:]
    sequence = f.readline()
    sequence = sequence.strip()
    cA = sequence.count('A')
    cC = sequence.count('C')
    cG = sequence.count('G')
    cT = sequence.count('T')
    cOthers = len(sequence) - (cA+cC+cG+cT)
    outfile.write(' '.join(map(str, [name, cA, cC, cG, cT, cOthers])) + '\n')
f.close()
outfile.close()

# Utilisation d'expression regulieres

import re

for fname in ["data1.txt", "data2.txt", "data3.txt", "data4.txt"]:
    f = open(fname)
    c1 = 0
    c2 = 0
    S = 0.
    D = 0.
    for line in f:
        c1 += 1
        match = re.search('S=(\d+)', line)
        S += int(match.group(1))
        match = re.search('D=(-?\d\.\d+)', line)
        if match != None:
            D += float(match.group(1))
            c2 += 1
    f.close()
    print fname
    print '   average S = {}'.format(1.*S/c1)
    print '   average D = {}'.format(1.*D/c2)
    print '   number of alignments without polymorphism: {}'.format(c1-c2)

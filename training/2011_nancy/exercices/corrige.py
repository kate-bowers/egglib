import egglib
import os

fnames = os.listdir('eagle_files')

aligns = {}
for fname in fnames:
    align = egglib.Align('eagle_files/' + fname)
    aligns[fname] = align

print type(aligns['aat'])
print aligns['aat'].ns(), aligns['aat'].ls()


group1 = "L0049B, L0144B, L0154C, L0163C, L0174B, L0198C, L0245B, L0263C, L0290B, L0310C, L0321B, L0337C, L0401C, L0530B, L0542B, L0549B, L0550B, L0552D, L0554B, L0555B, L0557B, L0651C, L0732D, L0734D, L0736D".split(', ')
group2 = "L0166B, L0213D, L0216B, L0233C, L0239B, L0306C, L0330B, L0357B, L0368D, L0369B, L0372B, L0400C, L0404B, L0410B, L0414C, L0421B, L0425C, L0430B, L0448B, L0482C, L0514C, L0526C, L0543B, L0544A, L0545C, L0546B, L0547B, L0640C, L0648D, L0673C, L0679C, L0738D".split(', ')
outgroup = "L0750D"

for name in aligns:
    for seq in aligns[name]:
        if seq.name in group1:
            seq.group = 1
        if seq.name in group2:
            seq.group = 2
        if seq.name == outgroup:
            seq.group = 999
            
print aligns['aat'].groups()

#print "name\tS\tD\tH\tFst\tfixed?"
for name in aligns:
    pol = aligns[name].polymorphism()
    S = pol["S"]
    if S>0:
        D = pol["D"]
        Fst = pol["Fst"]
        fixed = False
    else:
        D = "-"
        Fst = "-"
        fixed = True
    if "H" in pol:
        H = pol["H"]
    else:
        H = "-"
#    print name, "\t", S, "\t", D, "\t", H, "\t", Fst, "\t", fixed
    print name, "\t", S, "\t", pol['So']

